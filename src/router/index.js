import Vue from "vue";
import VueRouter from "vue-router";
import Home from "../views/Home.vue";
import Search from "../views/Search.vue";
import Playlists from "../views/Playlists.vue";
import AlbumDetails from "../views/AlbumDetails.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "",
    // redirect:'/search'
    redirect: { name: "Search" },
  },
  {
    path: "/search",
    name: "Search",
    component: Search,
  },
  {
    path: "/search/album/:album_id",
    name: "AlbumDetails",
    component: AlbumDetails,
    // beforeEnter(to,from,next){
    //   console.log(to,from); next
    // }
  },
  {
    path: "/home",
    name: "Home",
    component: Home,
  },
  {
    path: "/playlists",
    name: "Playlists",
    component: Playlists,
  },
  {
    path: "/home/about",
    name: "About",
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/About.vue"),
  },
  {
    path: "*",
    redirect: { name: "Search" },
  },
];

const router = new VueRouter({
  // mode: "hash",
  mode: "history",
  linkActiveClass: "active placki",
  linkExactActiveClass: "active active-exact",
  routes,
});

router.beforeEach((to, from, next) => {
  to, from, next
  // console.log(to, from);
  next();
});

router.onError((/* error */) => {
  // console.error(error);
});

export default router;
