let token = null;

const url = "https://accounts.spotify.com/authorize";

const client_id = "9fea088057144c28a1243ca29649d213";
const response_type = "token";
const redirect_uri = "http://localhost:8080/";
const scopes = [
  'playlist-read-private',
  'playlist-modify-private',
];
const show_dialog = "true";

const session = {
  user: null,
  token: "",
};

export const getSession = () => {
  return session;
};

export const autologin = () => {
  const jsonToken = sessionStorage.getItem("token");

  if (jsonToken) {
    token = JSON.parse(jsonToken);
  }

  if (!token && window.location.hash) {
    const p = new URLSearchParams(window.location.hash);
    token = p.get("#/access_token") || p.get("#access_token");
    window.location.hash = "#/";
  }

  if (!token) {
    authorize();
  } else {
    session.token = token;
    sessionStorage.setItem("token", JSON.stringify(token));
  }
};

export const authorize = () => {
  sessionStorage.removeItem("token");

  const p = new URLSearchParams({
    client_id,
    response_type,
    redirect_uri,
    scope: scopes.join(' '),
    show_dialog,
  });

  const redirect = `${url}?${p.toString()}`;

  window.location.href = redirect;
};

export const getToken = () => {
  return token;
};
