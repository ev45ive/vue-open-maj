import Vue from "vue";
import Vuex from "vuex";
import axios from "axios";

Vue.use(Vuex);

/* const fakeData = [
  {
    id: "1",
    name: "Vue Hits",
    public: false,
    description: "Opis",
  },
  {
    id: "2",
    name: "Vue Top 20",
    public: false,
    description: "Opis",
  },
  {
    id: "3",
    name: "Best of Vue",
    public: false,
    description: "Opis",
  },
]; */

const playlists = {
  namespaced:true,
  /* Data for editing */
  state: {
    list: [],
    selectedId: null,
    mode: "show",
    loading: false,
  },
  /* Data Logic - commit() */
  mutations: {
    setLoading(state, isLoading) {
      state.loading = isLoading;
    },
    select_playlist(state, playlistId) {
      state.selectedId = playlistId;
    },
    loadPlaylists(state, data) {
      state.list = data;
    },
    updatePlaylist(state, draft) {
      const index = state.list.findIndex((p) => p.id == draft.id);
      state.list.splice(index, 1, draft);
    },
    setMode(state, mode) {
      state.mode = mode;
    },
  },
  /* Business logic - dispatch() */
  actions: {
    edit({ commit }) {
      commit("setMode", "edit");
    },
    cancel({ commit }) {
      commit("setMode", "show");
    },
    create({ commit }) {
      commit("setMode", "create");
    },
    /**
     *  Load user playlists
     */
    fetchPlaylists({ commit }) {
      commit("setLoading", true);

      return axios.get("me/playlists").then((resp) => {
        commit("loadPlaylists", resp.data.items);
        commit("setLoading", false);
      });
    },
    /**
     * Create or update playlist
     */
    save({ commit, rootGetters, dispatch }, draft) {
      commit("setLoading", true);

      let requestPromise;

      if (draft.id) {
        requestPromise = axios.put("/playlists/" + draft.id, draft, {}); //
      } else {
        requestPromise = axios.post(
          "users/" + rootGetters.user.id + "/playlists",
          draft
        );
      }
      requestPromise
        .then(() => dispatch("fetchPlaylists"))
        .then(() => commit("setMode", "show"));
    },
    createPlaylist(/* { commit, state }, draft */) {},
    selectPlaylist({ commit, state }, playlist_id) {
      commit(
        "select_playlist",
        state.selectedId == playlist_id ? null : playlist_id
      );
    },
  },
  /* Data for View */
  getters: {
    loading: (state) => state.loading,
    playlists: (state) => state.list,
    selectedPlaylist: (state) =>
      state.list.find((p) => p.id == state.selectedId),
    mode: (state) => state.mode,
  },
};

const search = { state: {}, mutations: {} };
const users = { state: {}, mutations: {} };

export default new Vuex.Store({
  state: {
    user: null,
  },
  mutations: {
    setUser(state, user) {
      state.user = user;
    },
  },
  actions: {
    login({ commit }) {
      axios.get("me").then((resp) => {
        commit("setUser", resp.data);
      });
    },
  },
  getters: {
    user: (state) => state.user,
  },
  modules: {
    playlists,
    search,
    users,
  },
});
