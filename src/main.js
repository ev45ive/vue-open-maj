import Vue from "vue";
import App from "./App.vue";
import "./registerServiceWorker";
import router from "./router";
import store from "./store";
import VueResource from "vue-resource";
import { autologin, getToken, authorize } from "./services/auth";
import axios from "axios";

Vue.config.productionTip = false;

Vue.filter("yesno", (value) => (value ? "Yes" : "No"));
Vue.filter("uppercase", (value) => value.toUpperCase());

// import 'components/global.js';
// import PlaylistDetails from "./components/PlaylistDetails";
// Vue.component("AppCard", AppCard);
// Vue.component("MyButton", MyButton);

// const MyCostam = Vue.extend({})

// placki123@placki.com
// placki123


autologin();

axios.defaults.baseURL = "https://api.spotify.com/v1";
axios.interceptors.request.use((config) => {
  config.headers["Authorization"] = "Bearer " + getToken();
  return config;
});

/*  ==== */

Vue.use(VueResource, {});
Vue.http.options.root = "https://api.spotify.com/v1";
Vue.http.interceptors.push(function(request) {
  // modify headers
  request.headers.set("Authorization", "Bearer " + getToken());

  return (response) => {
    if (response.status == 401) {
      setTimeout(() => authorize(), 2000);
    }
    return response;
  };
});

window.Vue = Vue;
window.store = store;

new Vue({
  router,
  store,
  render: (h) => h(App),
  data: () => ({
    settings: {},
    theme: {},
    user: {},
  }),
}).$mount("#app");
